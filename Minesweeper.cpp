//Minesweeper(c++) - 2019 

#include <iostream>
#include <time.h>
#include <string>
#include <math.h>
#include <Windows.h>
#include <conio.h>
#include <ctime>
#include <stdlib.h>
#include <iomanip>
#include <chrono>

#define TABLE_SIZE 10
#define BOMBS 10
#define MAX_X 37
#define MAX_Y 19
clock_t startTime;
clock_t endTime;

using namespace std;

void gotoxy(int, int);
bool Valid(int, int);
void MakeBoard(char[][TABLE_SIZE]);
void MneBoard(char[][TABLE_SIZE]);
void VisitedBoard(char[][TABLE_SIZE]);
char ToChar(int);
bool GameOver(int, int, char[][TABLE_SIZE]);
bool WinGame(char[][TABLE_SIZE], char[][TABLE_SIZE]);
int NumberOfHome(int, int, char[][TABLE_SIZE]);
void OpenAdjacant(int, int, int, int, char[][TABLE_SIZE], char[][TABLE_SIZE], bool[][TABLE_SIZE]);
void ShowAllBombs(char[][TABLE_SIZE], char[][TABLE_SIZE]);
void MakeMove(int, int, char[][TABLE_SIZE], char[][TABLE_SIZE], bool[][TABLE_SIZE]);
void StartPlaying();
void Welcome();

//ADDITIONAL FUNCTIONS
void setFontSize(int);
void CountDown();
bool PlayAgain();
void duration(int);
void setTextColor(int, int);
void NumberColor(int);



int main()
{
	system("color f0");   //white background - black text
	setFontSize(20);      //FontSize = 20

	Welcome();

	system("pause>a");
}


void gotoxy(int column, int line)         //gotoxy func
{
	COORD coord;
	coord.X = column;
	coord.Y = line;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

bool Valid(int row, int col)    //func to see if the row & column are in the board 
{
	if (row < 0 || row >= TABLE_SIZE || col < 0 || col >= TABLE_SIZE)
		return false;
	return true;
}

void MakeBoard(char board[][TABLE_SIZE])           //func to make and display the board game  (1,1)-->(MAX_X,MAX_Y)   x+=4  &  y+=2
{

	for (int row = 0; row < TABLE_SIZE; row++)
		for (int col = 0; col < TABLE_SIZE; col++)
			board[row][col] = '*';

	cout << "\n" << " ";
	for (int row = 0; row < TABLE_SIZE; row++)
	{
		for (int col = 0; col < TABLE_SIZE; col++)
			cout << board[row][col] << "   ";
		cout << "\n\n ";
	}
	return;
}

void MineBoard(char mine[][TABLE_SIZE])     //func to make the arrey "mine", with random bombs
{
	srand(time(NULL));

	for (int row = 0; row < TABLE_SIZE; row++)
		for (int col = 0; col < TABLE_SIZE; col++)
			mine[row][col] = '0';

	int cnt = 0;
	while (cnt < BOMBS)
	{
		int x = rand() % TABLE_SIZE;
		int y = rand() % TABLE_SIZE;
		if (mine[x][y] == '0')
		{
			mine[x][y] = '#';
			cnt++;
		}
	}
}

void VisitedBoard(bool visited[][TABLE_SIZE]) //func to see if a home is already visited (used in open func)
{
	for (int row = 0; row < TABLE_SIZE; row++)
		for (int col = 0; col < TABLE_SIZE; col++)
			visited[row][col] = false;
}

char ToChar(int a)      //func to convert Int to Char 
{
	switch (a)
	{
	case 0:return '-';
	case 1:return '1';
	case 2:return '2';
	case 3:return '3';
	case 4:return '4';
	case 5:return '5';
	case 6:return '6';
	case 7:return '7';
	case 8:return '8';
	default: return 0;
	}
}

bool GameOver(int row, int col, char mine[][TABLE_SIZE])     //func to see if the clicked home is a bomb
{
	return (mine[row][col] == '#');
}

bool WinGame(char board[][TABLE_SIZE], char mine[][TABLE_SIZE])   //func to determine the Win situation
{
	int flag = 1;
	for (int row = 0; row < TABLE_SIZE; row++)
		for (int col = 0; col < TABLE_SIZE; col++)
			if ((board[row][col] == '*') && mine[row][col] != '#')
				flag = 0;
	return (flag == 1);
}

int NumberOfHome(int row, int col, char mine[][TABLE_SIZE])    //func to check out 8 adjacant homes and return the number for clicked home
{
	int cnt = 0;

	if (GameOver(row - 1, col - 1, mine) && Valid(row - 1, col - 1))
		cnt++;
	if (GameOver(row, col - 1, mine) && Valid(row, col - 1))
		cnt++;
	if (GameOver(row + 1, col - 1, mine) && Valid(row + 1, col - 1))
		cnt++;
	if (GameOver(row + 1, col, mine) && Valid(row + 1, col))
		cnt++;
	if (GameOver(row + 1, col + 1, mine) && Valid(row + 1, col + 1))
		cnt++;
	if (GameOver(row, col + 1, mine) && Valid(row, col + 1))
		cnt++;
	if (GameOver(row - 1, col + 1, mine) && Valid(row - 1, col + 1))
		cnt++;
	if (GameOver(row - 1, col, mine) && Valid(row - 1, col))
		cnt++;

	return cnt;
}

void OpenAdjacent(int row, int col, int x, int y, char mine[][TABLE_SIZE], char board[][TABLE_SIZE], bool visited[][TABLE_SIZE]) //func to open adjacent BOMBS
{
	if (!(Valid(row, col)))
		return;
	if (visited[row][col] && board[row][col] != 'F')
		return;

	board[row][col] = ToChar(NumberOfHome(row, col, mine));
	gotoxy(x, y);
	NumberColor(NumberOfHome(row,col,mine));  
	cout << board[row][col];
	visited[row][col] = true;

	if (NumberOfHome(row, col, mine) == 0)
	{
		OpenAdjacent(row + 1, col, x + 4, y, mine, board, visited);
		OpenAdjacent(row + 1, col - 1, x + 4, y - 2, mine, board, visited);
		OpenAdjacent(row, col + 1, x, y + 2, mine, board, visited);
		OpenAdjacent(row + 1, col + 1, x + 4, y + 2, mine, board, visited);
		OpenAdjacent(row - 1, col, x - 4, y, mine, board, visited);
		OpenAdjacent(row, col - 1, x, y - 2, mine, board, visited);
		OpenAdjacent(row - 1, col - 1, x - 4, y - 2, mine, board, visited);
		OpenAdjacent(row - 1, col + 1, x - 4, y + 2, mine, board, visited);
	}
}

void ShowAllBombs(char board[][TABLE_SIZE], char mine[][TABLE_SIZE])       //func to reveal all the bombs in case of loosing the game
{
	setTextColor(16, 15);
	for (int row = 0; row < TABLE_SIZE; row++)
		for (int col = 0; col < TABLE_SIZE; col++)
			if (mine[row][col] == '#')
				board[row][col] = mine[row][col];

	system("cls");
	cout << "\n" << " ";
	for (int row = 0; row < TABLE_SIZE; row++)
	{
		for (int col = 0; col < TABLE_SIZE; col++)
			cout << board[col][row] << "   ";
		cout << "\n\n ";
	}
}

void MakeMove(int x, int y, char board[][TABLE_SIZE], char mine[][TABLE_SIZE], bool visited[][TABLE_SIZE])    //func to move on the game board
{
	char command = 0;
	int row = 0, col = 0;     //These are set for "mine" , "board" & "visited" arreys

	while (true)
	{
		command = _getch();
		gotoxy(x, y);
		switch (tolower(command))
		{
			//UP
		case 'w':
			if (y == 1)
			{
				y = MAX_Y;
				col = 9;
				gotoxy(x, y);
			}
			else
			{
				gotoxy(x, y -= 2);
				col -= 1;
			}
			break;

			//DOWN
		case 's':
			if (y == MAX_Y)
			{
				y = 1;
				col = 0;
				gotoxy(x, y);
			}
			else
			{
				gotoxy(x, y += 2);
				col += 1;
			}

			break;

			//RIGHT
		case 'd':
			if (x == MAX_X)
			{
				x = 1;
				row = 0;
				gotoxy(x, y);
			}
			else
			{
				gotoxy(x += 4, y);
				row += 1;
			}

			break;

			//LEFT
		case 'a':
			if (x == 1)
			{
				x = MAX_X;
				row = 9;
				gotoxy(x, y);
			}
			else
			{
				gotoxy(x -= 4, y);
				row -= 1;
			}

			break;

			//FLAG
		case 'f':
			board[row][col] = 'F';
			gotoxy(x, y);
			setTextColor(14, 15); 
			cout << board[row][col];
			break;

			//RESTART
		case 'r':
			StartPlaying();
			break;

			//ENTER
		case 13:
			if (GameOver(row, col, mine))
			{
				endTime = time(NULL);

				ShowAllBombs(board, mine);
				cout << "GAME OVER :) \n";
				cout << " It Took About ";
				duration(difftime(endTime, startTime));
				cout << "\n\n";
				cout << " Wanna Play Again? (1/0) ";
				PlayAgain();
			}
			else
			{
				OpenAdjacent(row, col, x, y, mine, board, visited);
			}

			if (WinGame(board, mine))
			{
				endTime = time(NULL);

				Sleep(1000);
				system("cls");
				gotoxy(10, 10);
				cout << "YOU WON ^-^ \n";
				cout << " It Took About ";
				duration(difftime(endTime, startTime));
				cout << " For You To Win ;) ";
				cout << "\n\n";
				cout << " Wanna Play Again? (1/0): ";
				PlayAgain();
			}
			break;

		default:
			break;
		}
	}
}

void StartPlaying()
{
	system("cls");
	CountDown();
	system("cls");

	char board[TABLE_SIZE][TABLE_SIZE];
	char mine[TABLE_SIZE][TABLE_SIZE];
	bool visited[TABLE_SIZE][TABLE_SIZE];
	MakeBoard(board);
	MineBoard(mine);
	VisitedBoard(visited);

	int x = 1, y = 1;
	gotoxy(x, y);

	startTime = time(NULL);

	MakeMove(x, y, board, mine, visited);
}

void Welcome()
{
	int command = 0;
	cout << "Welcome To Minesweeper\n";
	cout << "To See The Instructions Press (1)\n";
	cout << "To Start The Game Press (2)\n";

	cin >> command;
	if (command == 1)
	{
		system("cls");
		cout << "To Move Left Press (A)\n";
		cout << "To Move Right Press (D)\n";
		cout << "To Move Up Press (W)\n";
		cout << "To Move Down Press (S)\n\n";

		cout << "To Set A Flag Press (F)\n\n";
		cout << "To Open A Home Press (ENTER)\n\n";
		cout << "To Restart The Game Press (R)\n\n";

		cout << "***** Press (2) To Start The Game *****\n";
		cin >> command;

		if (command == 2)
			StartPlaying();
	}

	else if (command == 2)
	{
		StartPlaying();
	}

}

//*********ADDITIONAL FEATURES********

void setFontSize(int FontSize)                    //font func
{
	CONSOLE_FONT_INFOEX info = { 0 };
	info.cbSize = sizeof(info);
	info.dwFontSize.Y = FontSize;
	info.dwFontSize.X = 0;
	info.FontWeight = FW_NORMAL;
	wcscpy_s(info.FaceName, L"Consolas");
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), NULL, &info);
}

void CountDown() //func to count from 3 to 1 (before starting)
{
	int a = 3;
	gotoxy(10, 10);
	cout << "start in ";

	while (a != 0)
	{
		cout << a;
		Sleep(1000);
		a--;
		cout << "\b";
	}
	return;
} 

bool PlayAgain()   //func to see if the user wants to play again
{
	int again;
	cin >> again;
	switch (again)
	{

	case 1:
		StartPlaying();
		break;

	case 0:
		exit(0);
		break;

	default:
		exit(0);
	}
}

void duration(int time)  //func to display the time
{
	int sec = 0, min = 0, h = 0;
	sec = time;
	while (sec >= 60)
	{
		min++;
		sec -= 60;
	}
	while (min >= 60)
	{
		h++;
		min -= 60;
	}
	cout << setfill('0') << setw(2) << h << ":";
	cout << setfill('0') << setw(2) << min << ":";
	cout << setfill('0') << setw(2) << sec;
}

void setTextColor(int textColor, int backColor)  //color func
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	int colorAttribute = backColor << 4 | textColor;
	SetConsoleTextAttribute(consoleHandle, colorAttribute);
}

void NumberColor(int a)  //func to set a specific color for a specific number
{
	if (a == 0)
		return setTextColor(16, 15);
	else if (a == 1)
		return setTextColor(9, 15);
	else if (a == 2)
		return setTextColor(18, 15);
	else if (a == 3)
		return setTextColor(12, 15);
	else if (a == 4)
		return setTextColor(17, 15);
	else if (a == 5)
		return setTextColor(4, 15);
	else if (a == 6)
		return setTextColor(19, 15);
	else if (a == 7)
		return setTextColor(16, 15);
	else if (a == 8)
		return setTextColor(8, 15);
}